﻿namespace Part3;

class Program
{
    static void Main(string[] args)
    {
        int[] array = { 1, 0, 0, 0, 1, 1, 0, 0, 0 };

        Console.WriteLine(CheckForOneSimple(array, 1, 3));
        Console.WriteLine(CheckForOneSimple(array, 1, 5));

        int[] prefixSum = CalculatePrefixSum(array);
        Console.WriteLine(CountOnesPrefixSum(prefixSum, 1, 5));
    }

    public static bool CheckForOneSimple(int[] array, int n, int m)
    {
        for (int i = n; i <= m; i++)
        {
            if (array[i] == 1)
            {
                return true;
            }
        }

        return false;
    }

    public static int CountOnesSimple(int[] array, int n, int m)
    {
        int count = 0;
        for (int i = n; i <= m; i++)
        {
            if (array[i] == 1)
            {
                count++;
            }
        }

        return count;
    }

    public static int[] CalculatePrefixSum(int[] array)
    {
        int[] prefixSum = new int[array.Length + 1];
        for (int i = 0; i < array.Length; i++)
        {
            prefixSum[i + 1] = prefixSum[i] + array[i];
        }

        return prefixSum;
    }

    public static bool CheckForOnePrefixSum(int[] prefixSum, int n, int m)
    {
        return (prefixSum[m + 1] - prefixSum[n]) > 0;
    }


    public static int CountOnesPrefixSum(int[] prefixSum, int n, int m)
    {
        return prefixSum[m + 1] - prefixSum[n];
    }

    // public static int CountOnesLinq(int[] array, int n, int m)
    // {
    //     return array.Skip(n).Take(m-n+1).Count(x => x == 1);
    // }
}