﻿using Problem2.EmailServices;
using Problem2.EmailTypes;

namespace Problem2;

public class Emailer
{
    private readonly IEmailService _emailService;

    public Emailer(IEmailService emailService)
    {
        _emailService = emailService ?? throw new ArgumentNullException(nameof(emailService));
    }

    public void SendResetPasswordEmail(IEmail email)
    {
        _emailService.SendEmail(email);
    }
}