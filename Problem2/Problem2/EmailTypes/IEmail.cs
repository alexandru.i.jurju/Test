﻿namespace Problem2.EmailTypes;

public interface IEmail
{
    public string Body { get; }
}