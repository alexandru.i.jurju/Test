﻿using Problem2.EmailTypes;

namespace Problem2.EmailServices;

public class OldEmailService : IEmailService
{
    public void SendEmail(IEmail email)
    {
        Console.WriteLine("Sent old email");
    }
}