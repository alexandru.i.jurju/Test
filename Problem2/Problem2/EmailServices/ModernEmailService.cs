﻿using Problem2.EmailTypes;

namespace Problem2.EmailServices;

public class ModernEmailService : IEmailService
{

    public void SendEmail(IEmail email)
    {
        Console.WriteLine("Sent modern email");
    }
}