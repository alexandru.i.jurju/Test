﻿using Problem2.EmailTypes;

namespace Problem2.EmailServices;

public interface IEmailService
{
    void SendEmail(IEmail email);
}