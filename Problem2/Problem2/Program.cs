﻿using Problem2.EmailServices;
using Problem2.EmailTypes;

namespace Problem2;

class Program
{
    static void Main(string[] args)
    {
        OldEmail oldEmail = new OldEmail();
        ModernEmail modernEmail = new ModernEmail();
        ModernEmailService modernEmailService = new ModernEmailService();
    }
}