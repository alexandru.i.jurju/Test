﻿using Problem1.Interfaces;

namespace Problem1;

public class CreditCardPayment : IPayment
{
    public string Name => "Card Payment";

    public void ProcessPayment()
    {
        Console.WriteLine("PAID WITH CARD");
    }
}