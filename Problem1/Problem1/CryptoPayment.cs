﻿using Problem1.Interfaces;

namespace Problem1;

public class CryptoPayment : IPayment
{
    public string Name => "CryptoPayment";

    public void ProcessPayment()
    {
        Console.WriteLine("PAID WITH CRYPTO");
    }
}