﻿using Problem1.Interfaces;

namespace Problem1;

public class PayPalPayment : IPayment
{
    public string Name => "PayPal";

    public void ProcessPayment()
    {
        Console.WriteLine("PAID WITH PAYPAL");
    }
}