﻿using Problem1.Interfaces;

namespace Problem1;

public class PaymentProcessor
{
    private readonly List<IPayment> _paymentMethods = new();


    public void ProcessPayment(int paymentMethodIndex)
    {
        if (paymentMethodIndex < 0 || paymentMethodIndex >= _paymentMethods.Count)
        {
            Console.WriteLine("Invalid payment method selected");
            return;
        }

        _paymentMethods[paymentMethodIndex].ProcessPayment();
    }

    public void DisplayAvailablePayments()
    {
        for (int i = 0; i < _paymentMethods.Count; i++)
        {
            Console.WriteLine(i + ": " + _paymentMethods[i].Name);
        }
    }

    public void AddPaymentMethod(IPayment paymentMethod)
    {
        _paymentMethods.Add(paymentMethod);
    }
}