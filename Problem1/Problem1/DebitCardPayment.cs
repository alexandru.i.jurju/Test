﻿using Problem1.Interfaces;

namespace Problem1;

public class DebitCardPayment : IPayment
{
    public string Name => "DebitCardPayment";

    public void ProcessPayment()
    {
        Console.WriteLine("PAID WITH CASH");
    }
}