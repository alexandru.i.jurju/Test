﻿namespace Problem1;

class Program
{
    static void Main(string[] args)
    {
        PaymentProcessor paymentProcessor = new PaymentProcessor();

        paymentProcessor.AddPaymentMethod(new CreditCardPayment());
        paymentProcessor.AddPaymentMethod(new DebitCardPayment());
        paymentProcessor.AddPaymentMethod(new PayPalPayment());
        paymentProcessor.AddPaymentMethod(new CryptoPayment());


        while (true)
        {
            Console.WriteLine("Please select a payment method:");
            paymentProcessor.DisplayAvailablePayments();

            if (int.TryParse(Console.ReadLine(), out var paymentMethodIndex))
            {
                paymentProcessor.ProcessPayment(paymentMethodIndex);
            }
            else
            {
                Console.WriteLine("Invalid input, please enter a number between 0 and 3.");
            }

            Console.WriteLine();
        }
    }
}