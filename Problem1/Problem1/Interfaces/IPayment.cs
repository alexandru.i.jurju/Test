﻿namespace Problem1.Interfaces;

public interface IPayment
{
    string Name { get; }
    void ProcessPayment();
}